#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
if sys.version_info[0] != 3 or sys.version_info[1] < 4:
    print("this script requires python version 3.4")
    sys.exit(1)

from os import path, listdir, mkdir, remove
from os.path import isfile, join, exists, splitext, isdir
import pymorphy2
import re
import codecs
import pickle
import math

def load_data(file_path):
    content = None
    with codecs.open(file_path, 'r', 'utf-8') as fp:
        content = fp.read()

    return content

def analyze_text(morph, text_data):
    stats = dict()
    lexemes = re.findall(r"\b[а-яА-Я]+\b", text_data)
    for idx, lex in enumerate(lexemes):
        word = morph.parse(lex)[0]
        normal_word = word.normal_form
        if normal_word in stats:
            stats[normal_word] += 1
        else:
            stats[normal_word] = 1
    return stats

def test(model, text_data):

    morph = pymorphy2.MorphAnalyzer()

    stats = analyze_text(morph, text_data)
    # model parameters
    alpha = 1.0

    cls_res_prob = {}
    for cls_name in model['class_prob']:
        class_prob = model['class_prob'][cls_name]
        prob_value = math.log(class_prob)

        for word in stats:
            if word in model['prob'][cls_name]['words_prob']:
                p = model['prob'][cls_name]['words_prob'][word]
            else:
                p = model['prob'][cls_name]['uknown_word_prob']

            prob_value += math.log(p)

        cls_res_prob[cls_name] = prob_value

    # printing results
    cls_labels = {1: 'политика', 2: 'спорт'}
    max_score = None
    max_label = None

    for cls_name in cls_res_prob:
        p = cls_res_prob[cls_name]
        label = cls_labels[cls_name]
        print('рубрика {}: {:.3f}'.format(label, p))

        if max_score is None:
            max_score = p
            max_label = label
        elif max_score < p:
            max_score = p
            max_label = label

    print('наиболее вероятная рубрика: {}'.format(max_label))

def load_model(model_path):
    model = None
    with open(model_path, 'rb') as fp:
        model = pickle.load(fp)
    return model

if __name__ == '__main__':


    if len(sys.argv) < 2:
        print('specify path to file with text, please.')
        print('usage: python3 test.py <path_to_txt>')
        exit(-1)

    model = load_model('model.p')
    data = load_data(sys.argv[1])
    test(model, data)