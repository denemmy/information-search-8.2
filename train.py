#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
if sys.version_info[0] != 3 or sys.version_info[1] < 4:
    print("this script requires python version 3.4")
    sys.exit(1)

from os import path, listdir, mkdir, remove
from os.path import isfile, join, exists, splitext, isdir
import pymorphy2
import re
import codecs
import pickle, json

def list_subdirs(base_dir):
    subdirs = list()
    for child in listdir(base_dir):
        subdir_path = join(base_dir, child)
        if isdir(subdir_path):
            subdirs.append(child)
    return subdirs

def list_txt(base_dir):
    images_list = []
    valid_exts = ['.txt']
    for f in listdir(base_dir):
        if not isfile(join(base_dir, f)):
            continue
        filext = splitext(f.lower())[1]
        if filext not in valid_exts:
            continue
        images_list.append(f)
    return images_list


def parse_class(filename):
    digits_only = ''.join([c for c in filename if c.isdigit()])
    number = int(digits_only)
    return number


def load_data(data_dir):
    cls_dirs = list_subdirs(data_dir)

    data = {}
    for cls_dir in cls_dirs:

        # class name
        cls_name = parse_class(cls_dir)
        print('loading class... {}'.format(cls_name))

        examples = []
        txt_files = list_txt(join(data_dir, cls_dir))

        for txt_file in txt_files:
            content = None
            with codecs.open(join(data_dir, cls_dir, txt_file), 'r', 'utf-8') as fp:
                content = fp.read()

            if content is not None:
                examples.append(content)
            else:
                print('warning: failed to read file {}'.format(txt_file))

        print('done. number of loaded examples for class {}: {}'.format(cls_name, len(examples)))
        data[cls_name] = examples

    print('finished loading data.')
    return data

def analyze_text(morph, text_data):
    stats = dict()
    lexemes = re.findall(r"\b[а-яА-Я]+\b", text_data)
    for idx, lex in enumerate(lexemes):
        word = morph.parse(lex)[0]
        normal_word = word.normal_form
        if normal_word in stats:
            stats[normal_word] += 1
        else:
            stats[normal_word] = 1
    return stats

def update_stats(stats_global, stats_local):
    for word in stats_local:
        if word in stats_global:
            stats_global[word] += stats_local[word]
        else:
            stats_global[word] = stats_local[word]

def train(data):

    print('run analyzer')
    morph = pymorphy2.MorphAnalyzer()

    stats_data = {}
    cls_docs_count = {}
    for cls_name in data:
        cls_data = data[cls_name]
        cls_stats = {}
        for example in cls_data:
            stats = analyze_text(morph, example)
            update_stats(cls_stats, stats)

        words_num = len(cls_stats.keys())
        print('class {}: number of words: {}'.format(cls_name, words_num))

        cls_docs_count[cls_name] = len(cls_data)
        stats_data[cls_name] = cls_stats

    print('finished analyzing.')

    print('training')
    # model parameters
    alpha = 1.0

    # computing vocabulary size
    all_words = {}
    for cls_name in stats_data:
        all_words.update(stats_data[cls_name])

    voc_size = len(all_words.keys())

    # computing probabilities
    prob_data = {}
    for cls_name in stats_data:
        cls_stats = stats_data[cls_name]

        # compute denominator
        cls_words_count = sum(cls_stats.values())
        denom = alpha * voc_size + cls_words_count

        uknown_word_prob = alpha / denom
        cls_prob = {}
        cls_prob['uknown_word_prob'] = uknown_word_prob

        words_prob = {}
        for word in cls_stats:
            prob = (cls_stats[word] + alpha) / denom
            words_prob[word] = prob

        cls_prob['words_prob'] = words_prob
        prob_data[cls_name] = cls_prob

    class_prob = {}
    total_docs_count = sum(cls_docs_count.values())
    for cls_name in cls_docs_count:
        class_prob[cls_name] = float(cls_docs_count[cls_name]) / total_docs_count

    print('training done.')

    print('writing model')

    model = {}
    model['alpha'] = alpha
    model['prob'] = prob_data
    model['class_prob'] = class_prob

    pickle.dump(model, open('model.p', 'wb'))

    # for visual viewing the model
    # with codecs.open('model.json', 'w', 'utf-8') as fp:
    #     json.dump(model, fp, indent=4, ensure_ascii=False)

    print('all done.')

    print(class_prob)

if __name__ == '__main__':

    data_dir = 'data'
    data = load_data(data_dir)
    train(data)